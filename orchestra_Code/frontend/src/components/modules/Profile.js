import React from "react";
import { Avatar } from "antd";
import { useSelector } from "react-redux";
import { Box } from "../elements/Box";

export function Profile() {
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const username = useSelector((state) => state.auth.username);

  return (
    <div style={{ width: "100%", height: "100%", backgroundColor: "#F5F5F5" }}>
      <div
        style={{
          marginLeft: collapsed ? "150px" : "297px",
          width: "100%",
          height: "100%",
          backgroundColor: "white",
          border: "1px",
          borderColor: "lightgrey",
        }}
      >
        <div style={{ marginLeft: "100px", fontStyle: "bold" }}>
          <h1>My Profile</h1>
        </div>
        <div marginleft="100px" display="inline-block">
          <div>
            <ul>
              <li>
                <div display="inline-block">
                  <p style={{ fontStyle: "bold", fontWeight: "bold" }}>
                    Name:{" "}
                  </p>{" "}
                  <p>{username}</p>
                </div>
              </li>
              <li>
                <p style={{ fontStyle: "bold", fontWeight: "bold" }}>
                  Instruments:
                </p>{" "}
              </li>
            </ul>
          </div>
          <div style={{ marginLeft: "20px" }}>
            <Avatar size={300} />
          </div>
        </div>
      </div>
    </div>
  );
}
