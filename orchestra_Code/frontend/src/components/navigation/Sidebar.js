import React, { useState } from "react";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import { Menu, Avatar } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  TeamOutlined,
  DashboardOutlined,
  SmileOutlined,
  SettingOutlined,
  ReadOutlined,
} from "@ant-design/icons";
import { useSelector } from "react-redux";

export function Sidebar() {
  const collapsed = useSelector((state) => state.navigation.collapsed);

  return (
    <div style={{ width: 256 }}>
      <Menu
        defaultSelectedKeys={["0"]}
        mode="inline"
        theme="dark"
        inlineCollapsed={collapsed}
        style={{
          position: "absolute",
          height: "100%",
          width: collapsed ? 75 : 256,
        }}
      >
        <div
          style={{
            marginLeft: "25px",
            marginTop: "20px",
            marginBottom: "15px",
          }}
        >
          <Avatar size={100} />
        </div>
        <Menu.Item key="0">
          <div>
            <Link to="/" />
            <DashboardOutlined />
            <span>Dashboard</span>
          </div>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to="/profile" />
          <SmileOutlined />
          <span>My Profile</span>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/groups" />
          <TeamOutlined />
          <span>My Groups</span>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/files" />
          <ReadOutlined />
          <span>My Files</span>
        </Menu.Item>
      </Menu>
    </div>
  );
}
