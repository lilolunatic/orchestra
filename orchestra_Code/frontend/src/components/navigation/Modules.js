import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import { Sidebar } from "./Sidebar";
import { Dashboard } from "../modules/Dashboard";
import { Profile } from "../modules/Profile";
import { Groups } from "../modules/Groups";
import { Files } from "../modules/Files";
import { Settings } from "../modules/Settings";
import PrivateRoute from "../common/PrivateRoute";
import store from "../../store";

export function Modules() {
  return (
    <div>
      <Provider store={store}>
        <Sidebar />
        <Router>
          <Switch>
            <PrivateRoute exact path="/" component={Dashboard} />
            <PrivateRoute exact path="/profile" component={Profile} />
            <PrivateRoute exact path="/groups" component={Groups} />
            <PrivateRoute exact path="/files" component={Files} />
            <PrivateRoute exact path="/settings" component={Settings} />
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}
