import React, { Fragment } from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";

import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

import Header from "./layout/Header";
import Login from "./accounts/Login";
import Register from "./accounts/Register";
import Alerts from "./layout/Alerts";
import PrivateRoute from "./common/PrivateRoute";

import { Provider, useSelector, useDispatch } from "react-redux";
import { Button } from "antd";

import store from "../store";
import { Sidebar } from "./navigation/Sidebar";
import { Dashboard } from "./modules/Dashboard";
import { Profile } from "./modules/Profile";
import { Groups } from "./modules/Groups";
import { Files } from "./modules/Files";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { setCollapsed } from "../actions/navigation";

//Alert Options
const alertOptions = {
  timeout: 3000,
  position: "top right",
};

export default function App() {
  const auth = useSelector((state) => state.auth);
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const dispatch = useDispatch();

  function toggleCollapsed() {
    dispatch(setCollapsed(!collapsed));
  }

  const { user } = auth;
  return (
    <Provider store={store}>
      <AlertProvider template={AlertTemplate} {...alertOptions}>
        <Router>
          <Fragment>
            <Header />
            <Alerts />
            <div
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: "#F5F5F5",
              }}
            >
              {user ? (
                <>
                  <Button
                    type="primary"
                    onClick={toggleCollapsed}
                    style={{ marginBottom: 16 }}
                  >
                    {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                  </Button>
                  <Sidebar />
                  <Switch>
                    <PrivateRoute
                      exact
                      path="/dashboard"
                      component={Dashboard}
                    />
                    <PrivateRoute exact path="/profile" component={Profile} />
                    <PrivateRoute exact path="/groups" component={Groups} />
                    <PrivateRoute exact path="/files" component={Files} />
                  </Switch>
                </>
              ) : (
                <>
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/login" component={Login} />
                </>
              )}
            </div>
          </Fragment>
        </Router>
      </AlertProvider>
    </Provider>
  );
}
