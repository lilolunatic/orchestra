import { SET_COLLAPSED } from "../actions/types";

const initialState = {
  collapsed: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_COLLAPSED:
      console.log("HI");
      return {
        ...state,
        collapsed: action.payload,
      };

    default:
      return {
        ...state,
      };
  }
}
