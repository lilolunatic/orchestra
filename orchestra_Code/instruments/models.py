from django.db import models


class Instrument(models.Model):
    instrument = models.CharField(max_length=100, unique=True)
