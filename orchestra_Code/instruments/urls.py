from django.urls import path, include
from knox import views as knox_views
from rest_framework import routers

from .api import InstrumentViewSet
from . import views

router = routers.DefaultRouter()
router.register('api/instruments', InstrumentViewSet, 'instruments')

urlpatterns = router.urls
