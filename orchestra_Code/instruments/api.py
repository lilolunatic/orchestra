from rest_framework import generics, permissions, viewsets
from knox.models import AuthToken
from .serializers import InstrumentSerializer
from .models import Instrument


class InstrumentViewSet(viewsets.ModelViewSet):

    permission_classes = [
        permissions.IsAuthenticated,
    ]

    serializer_class = InstrumentSerializer

    # request works despite error ¯\_(ツ)_/¯
    queryset = Instrument.objects.all()

    def perform_create(self, serializer):
        serializer.save()
