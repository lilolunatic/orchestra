from rest_framework import generics, permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from knox.models import AuthToken
from .serializers import UserSerializer, RegisterSerializer, LoginSerializer, ProfileSerializer
from instruments.models import Instrument
from django.http import JsonResponse
import json

# Register API


class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# Login API
class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# Get User API
class UserAPI(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


# # Request ProfileClass
# class ProfileViewSet(viewsets.ModelViewSet):
#     permission_classes = [
#         permissions.IsAuthenticated
#     ]

#     serializer_class = ProfileSerializer

#     def get_object(self):
#         profile = self.request.user.profile
#         print("_____________________________________________________")
#         return profile


# Get Profile
class ProfileViewSet(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['get', 'head']

    serializer_class = ProfileSerializer

    def get(self, request):
        user = self.request.user

        allInstruments = []
        userInstruments = []

        for i_N in Instrument.objects.all().values("instrument"):
            allInstruments.append(i_N["instrument"])

        for u_I in Instrument.objects.filter(
                profile=user.profile).values("instrument"):
            userInstruments.append(u_I["instrument"])

        instrumentDict = {}

        for instrumentName in allInstruments:
            if instrumentName in userInstruments:
                instrumentDict.update({instrumentName: True})
            else:
                instrumentDict.update({instrumentName: False})

        return JsonResponse({
            "user": [user.id, user.username],
            # {"flute": True, "piano":False, "trumpet": True}
            "instruments": instrumentDict
        })
