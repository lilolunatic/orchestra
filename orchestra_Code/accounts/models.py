from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from instruments.models import Instrument


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    image = models.ImageField(default='default.png', upload_to='profile_pics')
    instruments = models.ManyToManyField(
        Instrument, blank=True)  # blank needed?

    @classmethod
    def addInstrument(cls, request, name):
        instrument = get_object_or_404(Instrument, name=name)
        profile = request.user.profile
        profile.instruments.add(instrument)
        profile.save()

    @classmethod
    def removeInstrument(cls, request, name):
        instrument = get_object_or_404(Instrument, name=name)
        profile = request.user.profile
        profile.instruments.delete(instrument)
        profile.save()
