# # Use-Case Specification: Manage User Instruments of Group

# 1. Manage Assignment

## 1.1 Brief Description
A user can be in diffrent groups and play diffrent instruments in each of them. Therefore every user can select his instruments in each group.
## 1.2 Mockup
![Mockup](../Mockups/Mange_Instruments_of_Group.png)

## 1.3 Screenshot
![Screenshoot](../UCScreenshots/UC_Mange_User_Instruments_of_Group.JPG)

# 2. Flow of Events
![Activity Diagram](../CRUDs/CRUD_Manage_Instruments.png)
## 2.1 Basic Flow
![Activity Diagram](../activityDiagrams/Manage_Instruments_of_Group.png)


### Activity Diagram


## 2.2 Alternative Flows
n/a

# 3. Special Requirements
n/a

# 4. Preconditions
The  preconditions for this use case are:

 1. The user is logged in.
 2. The user is in a group lobby


# 5. Postconditions
n/a


# 6. Function Points
| Measurement Parameter         | RET | DET | FTR |
|-------------------------------|-----|-----|-----|
| External Inputs               |     | 4   | 1   |
| External Outputs              |     | 1   | 1   |
| External Queries              |     | 1   | 1   |
| External Interface Files      | 0   | 0   | 0   |
| Internal Logic Files          | 2   | 2   | 0   |

Function Points: 18,12

# 7. Gherkin
The corresponding feature file for our gherkin test can be found ![here](https://gitlab.com/lilolunatic/orchestra-code/-/blob/master/features/SetUserInstrumentsGroup.feature).
