# # Use-Case Specification: Upload profile Image
# 1. Upload profile Image

## 1.1 Brief Description
In his or her profile a user can upload a profile picture that will be displayed as an avatar on the side and in groups.
If no custom profile image is uploaded, there's a default image that is displayed.

## 1.2 Mockup
![Mockup](../Mockups/Profile_Picture_Mockup.png)

## 1.3 Screenshot
![Screenshot](../UCScreenshots/UC_Profile_Image_default.png)
![Screenshot](../UCScreenshots/UC_Profile_Image_Selection.png)
![Screenshot](../UCScreenshots/UC_Profile_Image_changed.png)
# 2. Flow of Events

## 2.1 Basic Flow

### Activity Diagram

![Activity Diagram](../activityDiagrams/ProfilePicture.png)


## 2.2 Alternative Flows
n/a

# 3. Special Requirements
n/a
# 4. Preconditions
The  preconditions for this use case are:

 1. The user is logged in.
 2. The user is on his profile.


# 5. Postconditions
The user is on his or her profile and the profile image is displayed in the upload-area as well as the Sidebar.


# 6. Function Points
# 6. Function Points

| Measurement Parameter         | RET | DET | FTR |
|-------------------------------|-----|-----|-----|
| External Inputs               |     | 4   | 1   |
| External Outputs              |     | 1   | 1   |
| External Queries              |     | 1   | 1   |
| External Interface Files      | 0   | 0   | 0   |
| Internal Logic Files          | 1   | 2   | 0   |

Function Points: 16.25

# 7. Gherkin
The corresponding feature file can be found ![here](https://gitlab.com/lilolunatic/orchestra-code/-/blob/master/features/ProfileImage.feature).

