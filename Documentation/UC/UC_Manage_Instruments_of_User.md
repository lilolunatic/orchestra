# # Use-Case Specification: Manage Instruments of User

# 1. Manage User Instruments

## 1.1 Brief Description
In his or her profile, a user can select instruments he or she is playing. Depending on this selection different sheets are assinged to him or her.
In this usecase it is described how the user can edit the selection of instruments.

## 1.2 Mockup
![Mockup](../Mockups/Mange_Instruments_of_User.png)

## 1.3 Screenshot
![Screenshot](../UCScreenshots/UC_Manage_User_Instruments.JPG)

# 2. Flow of Events

## 2.1 Basic Flow

### Activity Diagram

![Activity Diagram](../activityDiagrams/Manage_User_Instruments.png)


## 2.2 Alternative Flows
n/a

# 3. Special Requirements
n/a
# 4. Preconditions
The  preconditions for this use case are:

 1. The user is logged in.
 2. The user is on his profile.


# 5. Postconditions
The user is on his or her profile and the new selection is displayed.


# 6. Function Points
| Measurement Parameter         | RET | DET | FTR |
|-------------------------------|-----|-----|-----|
| External Inputs               |     | 4   | 1   |
| External Outputs              |     | 1   | 1   |
| External Queries              |     | 1   | 1   |
| External Interface Files      | 0   | 0   | 0   |
| Internal Logic Files          | 1   | 2   | 0   |

Function Points: 18,12

# 7. Gherkin
The corresponding feature file can be found ![here](https://gitlab.com/lilolunatic/orchestra-code/-/blob/master/features/SetUserInstruments.feature).

