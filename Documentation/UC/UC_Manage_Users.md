# # Use-Case Specification: Manage Users

# 1. Manage Users

## 1.1 Brief Description
In the application groups with several users can be created. During and after the creation there's the possibility to add people to the group or remove them. Furthermore, the
permissions in the group can be edited. 
There are three roles, three types of permissions: 
  * Musician: this is the standard, pre-selected role a new person gets in the group. A person with this role has reading-permissions and can make personal comments.
  * Music-admin: a person with this role has more permissions. He or she can add new people to the group, and make notes and changes in the sheet music, that are distributed to everyone in the group.
  * Conductor: the conductor has the most permissions. He can add or delete people, set and edit permissions, make changes in the sheet music and upload new sheet music. There can be more than one conductor. A person has this permissions automatically if he or she is the creator of this group.
  

## 1.2 Mockup
![Mockup](../Mockups/Mockup_Manage_Useres.png)
## 1.3 Screenshot
![Screenshot](../UCScreenshots/UC_Manage_Users.png)


# 2. Flow of Events
![CRUD](../CRUDs/CRUD Manage Users.png)

## 2.1 Basic Flow

### Activity Diagram

![Activity Diagram](../activityDiagrams/ManageUsers.png)


## 2.2 Alternative Flows
n/a

# 3. Special Requirements
To manage the users of a group, a user has to have the permissions of a conductor.

# 4. Preconditions
The  preconditions for this use case are:

 1. The user is logged in.
 2. The user is in the group lobby or creating a new group.


# 5. Postconditions
User is in the Group Lobby


# 6. Function Points
| Measurement Parameter         | RET | DET | FTR |
|-------------------------------|-----|-----|-----|
| External Inputs               |     | 5   | 1   |
| External Outputs              |     | 2   | 1   |
| External Queries              |     | 3   | 2   |
| External Interface Files      | 0   | 0   | 0   |
| Internal Logic Files          | 1   | 2   | 0   |

Function Points: 29,9

# 7. Gherkin
The corresponding feature file can be found ![here](https://gitlab.com/lilolunatic/orchestra-code/-/blob/master/features/ManageUsers.feature).

