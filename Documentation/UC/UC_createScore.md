### Use-Case Specification: Create Score

# 1. Create Score

## 1.1 Brief Description
In this usecase the conductor can add music works for several instrumetns. It is described how xml sheet music can be added and how it will be processed.
## 1.2 Mockup
![Mockup](../Mockups/manage_score_ii.png)
## 1.3 Screenshot
![Screenshot](../UCScreenshots/UC_Create_Score.JPG)
# 2. Flow of Events

## 2.1 Basic Flow 
 
### Activity Diagram
![Activity Diagram](../activityDiagrams/CreateScore.png)
## 2.2 Alternative Flows
n/a
# 3. Special Requirements
n/a
# 4. Preconditions
The preconditions for this use case are:
1.	The user is logged in.
2.	The user is in a group lobby
3.	The user has the role Conductor in the group.

# 5. Postconditions
1. The user is redirected to the score overview of the new score.
2. The score with its sheets is uploaded and visible in the group lobby.

# 6. Function Points
# 6. Function Points
| Measurement Parameter         | RET | DET | FTR |
|-------------------------------|-----|-----|-----|
| External Inputs               |     | 3   | 1   |
| External Outputs              |     | 2   | 1   |
| External Queries              |     | 3   | 2   |
| External Interface Files      | 0   | 0   | 0   |
| Internal Logic Files          | 1   | 2   | 0   |

Function Points 27.3

# 7. Gherkin
The corresponding feature file can be found ![here](https://gitlab.com/lilolunatic/orchestra-code/-/blob/master/features/CreateScore.feature).
