# # Use-Case Specification: Create Group

# 1. Create Group

## 1.1 Brief Description
One basic action in the orchestra application is the creation of groups.
The user can create a group and add the members of his or her orchestra, band or any other musical project to the group.
Afterwards he or she can manage people, permissions of the group, described in our usecase "Manage Users"

## 1.2 Mockup

![Mockup create groupe][create-groupe]

View after group creation:
![Mockup create groupe][groupe-lobby-conductor]

## 1.3 Screenshot
View for group creation:
![Screenshot](../UCScreenshots/UC_Create_Group.JPG)

Click on the plus in your group view to get to group creation:
![Screenshot](../UCScreenshots/UC_CreateGroup_Enter.png)

# 2. Flow of Events

## 2.1 Basic Flow
[Feature File](https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/Narratives/Create%20Group.feature)

### Activity Diagram
Group creator:

![Activity Diagram](../activityDiagrams/Create Group.png)

Manage Users:

![Activity Diagram](../activityDiagrams/ManageUsers.png)

## 2.2 Alternative Flows
n/a

# 3. Special Requirements
n/a

# 4. Preconditions
The  preconditions for this use case are:

 1. The user is logged in.
 2. The user is in the group overview.


# 5. Postconditions
 2. The user is in the group lobby.


# 6. Function Points
| Measurement Parameter         | RET | DET | FTR |
|-------------------------------|-----|-----|-----|
| External Inputs               |     | 5   | 2   |
| External Outputs              |     | 2   | 1   |
| External Queries              |     | 2   | 2   |
| External Interface Files      | 0   | 0   | 0   |
| Internal Logic Files          | 1   | 2   | 0   |

Function Points: 31,2


[create-groupe]: ../Mockups/create_group.png "Mockup Create groupe use case"
[groupe-lobby-conductor]: ../Mockups/group_lobby_-_conductor_view.png "Mockup groupe lobby conductor"

# 7. Gherkin
The corresponding feature file can be found ![here](https://gitlab.com/lilolunatic/orchestra-code/-/blob/master/features/CreateGroup.feature).