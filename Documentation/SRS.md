# Orchestra - Software Requirements Specification Document

| **Date**   | **Version** | **Description**                    | **Author**    |
| ---------- | ----------- | ---------------------------------- | ------------- |
| 15/10/2019 | 1.0         | Initial Requirements Specification | Nina Hartmann |
| 27/04/2020 | 1.0         | Revision                           | Nina Hartmann |
| 28/06/2020 | 1.0         | Revision for presentation          | Nina Hartmann |
|            |             |                                    |               |

Software Requirements Specification

## Introduction

### Purpose

The purpose of this document is to provide a general overview over the
requirements of the Orchestra-Project. It will broken down from the vision to
the main features and the specific requirements for their realization. It also
shows the boundaries of the project and contains information about the
development process.

### Scope

The Orchestra-application is going to be realized as a web-application.

Every user can create an account as well as create and join groups. In such a
group each user can have different roles and depending on the role the
permissions differ.

The main features of the application are:

- Creating and managing groups

- Importing sheet music

- Distributing sheet music in an orchestra or among a group of musicians

### Definitions, Acronyms, and Abbreviations

To be determined

### References

| Title                                                                                                  | Date       |
| ------------------------------------------------------------------------------------------------------ | ---------- |
| [Blog](https://orchestraapplication.wordpress.com/)                                                    | 15/10/2019 |
| [Gitlab Documentation](https://gitlab.com/lilolunatic/orchestra/-/tree/master/Documentation)           | 28/06/2019 |
| [Gitlab Code](https://gitlab.com/lilolunatic/orchestra-code)                                           | 24/06/2019 |
| [Use case diagram](https://drive.google.com/file/d/1lPRx8rUuYBwokfRQHjZdt6uM40AqgpaF/view?usp=sharing) | 15/10/2019 |
| [Heroku App](https://orchestra-se.herokuapp.com/#/login)                                               | 28/06/2020 |



### Overview

The following document provides an overview with our vision and a detailed view
of each feature. In the following we provide more information about the
attributes of the Orchestra-application regarding requirements, functionality,
performance, and more.

## Overall Description

### Vision

We want to create an application for musicians that helps them to organize their
projects and sheet music. The application will be accessible online. After the
users created an account they can create groups to organize their projects. In
this group they can upload their sheet music in a MusicXML-based format and
distribute different sections to the different members of the group. The
application also provides a function for the distribution of permissions in the
group.

If the musicians agree on a way to interpret a piece, they can make entries in
the sheet music which are then distributed to everyone that’s affected.

Last but not least the application can be used in a rehearsal in a simple
reading-view.

### User characteristics

The target group for this application are musicians that play in one or several
projects, like bands, orchestras, or other groups. With this application the
administration of such groups shall be simplified and misunderstandings shall be
avoided.

### Use Case Diagram

![logo](usecases.png "UCD")



### Dependencies

The application supports only MusicXML-based formats. Furthermore the
editing-functionality will be embedded, so it depends on an open-source editing
tool that is going to be announced in the future.

## Specific Requirements

### Functionality

#### User administration

After a user has opened our website, he can either register or (if he already
has an account) log in. After logging in, he can log out. Logging in is the
prerequisite to use all other usecases. After logging in the user gets to the
dashboard for the group administration.

the specification for the users instruments can be seen
![here][instrumentAdministrationGroup]

[instrumentAdministrationGroup]: https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_Manage_Instruments_of_User.md

the specification for the users profile image can be seen
![here][profileImage]

[profileImage]:https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_upload_profile_image.md

#### Group administration

In the dashboard all groups (in which the user is) are displayed. From here the
user can enter or create a group. Then the user is in the group lobby. A member
in a group can have one of three roles, these are arranged hierarchically. A
higher role also has the rights of the lower role. The roles are (listed in
descending order) Conductor, Music Admin and Musican. Furthermore the Conductor
can configure the roles of all other members in this group.

the specification can be found 
![here][groupadministration]

[groupadministration]: https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_Manage_Users.md

the specification for the administration can be found 
![here][groupadministration]

[groupadministration]: https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_Create_Group.md

#### Distribution

In the group lobby, the Conductor can import a Music XML file and then
distribute it to the group. He can also push all members of the group to one
position in the sheet of music. So that this place is displayed at all of them
(if they have opened the sheet).

the specification for the import can be found 
![here][import]

[import]: https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_createScore.md

the specification for the file-assignment can be found 
![here][assignment]

[assignment]: https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/Manage_User_Instruments_of_Group.md


#### Editing

A sheet can be edited or notes added to it.

#### Display sheet

In the group lobby you can select a sheet and then view it.

### Usability

The application shall be clearly structured and self-explanatory, so it can be used by everyone without questions.
To ensure this, we will have a user test it and give us feedback on the design.

### Reliability

The application shall be constantly accessible for the user. It’s important that
the users have always access to their sheet music. Maintenance times will be
kept as short as possible and changes shall be implemented over night or with
agreement of the users.

For safety and daily use, there can be local copies of the sheet music on the
user’s device. The synchronization can be paused, in case of maintenance, but no
data will be lost that way.

### Performance

The software and the web-server behind it shall be available constantly.
Dependent on the selection of a web-server we can provide a short response time.
By building a functional architecture in which the data are stored, we hope to
minimize transfer and response time, even if several users access their data at
the same time. However, the application is not as depending on fast
response-times. Nevertheless, we want to provide good service by using a server
in a big data center in, for example, Frankfurt.

### Supportability

To keep the code maintainable, we will follow some coding conventions regarding
naming, language and will clearly divide tasks to keep the classes short.
Additionally, there will be a solid documentation.

### Design Constraints

We use the MVC architecture.

#### MusicXML

MusicXML is an data-format in which sheet music is saved.

#### SQLite

As database we are going to use a SQLite relational database. It’s the default Database used in combination with Django.

#### Python

Python is an object oriented, scriptbased language.

#### Redux

It is a predictable state container for JavaScript applications.

#### JSX

JSX is a syntax used in ReactJS. It is a JavaScript syntax extension, that mimics html tags.

#### ReactJS

ReactJS is a Frontend Framework that works with JavaScript.

#### Axure

Axure RP 9 is a tool to create user mockups of interfaces.

#### Codacy

With Codacy we can measure our code.

#### Heroku

Heroku is our deployment tool.

#### postgres

postgres is the database, that works with aur deployment tool heroku.

### gherkin/behave

We use Gherkin and behave to write behaviour-driven tests and their step definitions.

### Selenium Web driver

Selenium comes into play when you want to automate your UI-tests in browser. 
With Selenium Web Driver we execute our behaviour driven tests. It takes control over the browser.

### On-line User Documentation and Help System Requirements

To be determined.

### Purchased Components

Not applicable.

### Interfaces

To be determined.

### Licensing Requirements

Not applicable

### Legal, Copyright, and Other Notices

To be determined.

### Applicable Standards

To be determined.

## Supporting Information

To be determined.
