# Test plan

## 1.	Introduction
### 1.1	Purpose
The purpose of the Iteration Test Plan is to gather all of the information necessary to plan and control the test effort for a given iteration. 
It describes the approach to testing the software.
This Test Plan for **Orchestra** supports the following objectives:

Unit testing with Django, frontend testing with Jest and API testing with Django.

### 1.2	Scope
This document describes the used test, as they are unit tests.

### 1.3	Intended Audience
Primarily for developer. 
### 1.4	Document Terminology and Acronyms
- **SRS**	Software Requirements Specification
- **n/a**	not applicable
- **tbd**	to be determined

### 1.5	 References
n/a

            
## 2.	Evaluation Mission and Test Motivation
### 2.1	Background
Since we can all make mistakes, testing helps to detect them early during development.

### 2.2	Evaluation Mission
* Find Bugs
* Verify functionality

### 2.3	Test Motivators
Our testing is motivated by:
- quality risks
- technical risks
- use cases
- functional requirements

## 3.	Target Test Items
The listing below identifies those test items (software, hardware, and supporting product elements) that have been identified as targets for testing. 
This list represents what items will be tested. 

Items for Testing:
- Orchestra Backend (Django)
- Orchestra Frontend (Jest)
- UI (Gherkin)

## 4.	Outline of Planned Tests
### 4.1	Outline of Test Inclusions
n/a
### 4.2	Outline of Other Candidates for Potential Inclusion
n/a
### 4.3 Outline of Test Exclusions
n/a
## 5.	Test Approach
see 1.0
### 5.1 Initital Test-Idea Catalogs and Other Reference Sources
**n/a**
### 5.2	Testing Techniques and Types

#### 5.2.1 Orchestra Unit Testing
|| |
|---|---|
|Technique Objective  	| Several functions in the backend will be called, their results will be compared with predefined results |
|Technique 		| Data should be mocked. Backend will not be started, no datastore will be used |
|Oracles 		| Functions return the correct and expected data. |
|Required Tools 	| jest, Django|
|Success Criteria	| expected responses, passing tests |
|Special Considerations	|     -          |


#### 5.2.1 Orchestra API Testing
|| |
|---|---|
|Technique Objective  	| The backend should be started. Several request should be made to determine the correct functionality of the API. |
|Technique 		|  Data should be mocked. A temporary in-memory database will be used to not expose unnecessary load onto the production database. |
|Oracles 		| Endpoints return the correct and expected data as well as the expected response codes. |
|Required Tools 	| Django |
|Success Criteria	| expected responses, passing tests |
|Special Considerations	|     -          |

#### 5.2.2 UI Frontend Testing
|| |
|---|---|
|Technique Objective  	| Every interaction with the frontend should function as intended. |
|Technique 		| Gherkin tests should test the technology. The backend is simulated in-memory by the frontend.  |
|Oracles 		| backend returns expected data, information is shown as expected |
|Required Tools 	| Gherkin |
|Success Criteria	| Expected behavior and passing tests |
|Special Considerations	|     -          |

#### 5.2.3 Business Cycle Testing
**n/a**

#### 5.2.4 User Interface Testing
**n/a**

#### 5.2.5 Performance Profiling 
**n/a**

#### 5.2.6 Load Testing
**n/a**

#### 5.2.7 Stress Testing
**n/a**

#### 5.2.8	Volume Testing
**n/a**

#### 5.2.9	Security and Access Control Testing
**n/a**

#### 5.2.10	Failover and Recovery Testing
**n/a**

#### 5.2.11	Configuration Testing
**n/a**

#### 5.2.12	Installation Testing
**n/a**

## 6.	Entry and Exit Criteria
### 6.1	Test Plan
#### 6.1.1	Test Plan Entry Criteria
Gitlab detects a push
#### 6.1.2	Test Plan Exit Criteria
When all tests pass without throwing an exception.
#### 6.1.3 Suspension and Resumption Criteria
n/a

## 7.	Deliverables
### 7.1	Test Evaluation Summaries
Whether a test has failed can be checked in the gitlab pipline
### 7.2	Reporting on Test Coverage
Backend Coverage can be reported from Pycharm. Frontend Coverage can be reported from jest
### 7.3	Perceived Quality Reports
n/a
### 7.4	Incident Logs and Change Requests
n/a
### 7.5	Smoke Test Suite and Supporting Test Scripts
n/a
### 7.6	Additional Work Products
#### 7.6.1	Detailed Test Results
The detailed test results from the frontend, are available in the code repository (in src/coverage). 

#### 7.6.2	Additional Automated Functional Test Scripts
n/a
#### 7.6.3	Test Guidelines
n/a
#### 7.6.4	Traceability Matrices
n/a

## 8.	Testing Workflow
Developers should execute tests locally before pushing source code. When pushing, tests are executed automatically.
## 9.	Environmental Needs
This section presents the non-human resources required for the Test Plan.
### 9.1	Base System Hardware
The following table sets forth the system resources for the test effort presented in this Test Plan.

n/a

### 9.2	Base Software Elements in the Test Environment
The following base software elements are required in the test environment for this Test Plan.

n/a

### 9.3	Productivity and Support Tools
The following tools will be employed to support the test process for this Test Plan.

| Tool Category or Type | Tool Brand Name                              |
|-----------------------|----------------------------------------------|
| CI Service            | [Gitlab CI](https://gitlab.com/lilolunatic/orchestra-code/pipelines) |

### 9.4	Test Environment Configurations
n/a

## 10.	Responsibilities, Staffing, and Training Needs
### 10.1	People and Roles
This table shows the staffing assumptions for the test effort.

Human Resources


| Role | Minimum Resources Recommended (number of full-time roles allocated) |	Specific Responsibilities or Comments |
|---|---|---|
| Test Manager | 1 | Provides management oversight. <br> Responsibilities include: <br> planning and logistics <br> agree mission <br> identify motivators<br> acquire appropriate resources<br> present management reporting<br> advocate the interests of test<br>evaluate effectiveness of test effort |
| Test Designer | 1 | Defines the technical approach to the implementation of the test effort. <br> Responsibilities include:<br> define test approach<br> define test automation architecture<br> verify test techniques<br> define testability elements<br> structure test implementation|
| Tester | 3 |	Implements and executes the tests.<br> Responsibilities include:<br> implement tests and test suites<br> execute test suites<br> log results<br> analyze and recover from test failures<br> document incidents|
| Implementer | 3 | Implements and unit tests the test classes and test packages.<br> Responsibilities include:<br> creates the test components required to support testability requirements as defined by the designer |

### 10.2	Staffing and Training Needs
**n/a**
## 11.	Iteration Milestones

| Milestone | Planned Start Date | Actual Start Date | Planned End Date | Actual End Date |
|---|---|---|---|---|
| Have Unit Tests | 06.05.2020 | 06.05.2020 | 04.06.2020 | 12.05.2020  |
| 50% coverage | 06.05.2020 | 06.05.2020 | 16.06.2020 |16.06.2020 |
| Tests integrated in CI | 06.05.2020 | 06.05.2020 | 12.05.2020 | 15.05.2020 |


## 12.	Risks, Dependencies, Assumptions, and Constraints
| Risk | Mitigation Strategy	| Contingency (Risk is realized) |
|---|---|---|
| Undetected Bugs | Reach high code coverage with test | Patches, rollback deployment to last stable version |
| Bugs in tests | keep tests simple | Identify the bug source and fix it |
## 13. Management Process and Procedures

### 13.1 Deployment (identical for all components)
1. Run Codacy to find metrics
2. Run GitLab CI (builds the software, runs all tests)
3. Run GitLab CD to Deploy on Heroku
