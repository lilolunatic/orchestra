| **Date**   | **Version** | **Description**                    | **Author**    |
|------------|-------------|------------------------------------|---------------|
| 23/11/2019 | 1.0         | Initial description of architecture| Nina Hartmann |
| 01/12/2019 | 1.1         | UC-diagram update and              | Nathalie Böhm |
|            |             | Manage Instruments                 |               |
| 20/06/2020 | 1.2         | UC and deploy update               | Manuel Luther |
| 28/06/2020 | 1.3         | Adding Metrics                     | Nina Hartmann |
| 28/06/2020 | 1.4         | Update Klass Diagramms             | Manuel Luther |

# Software Architecture Document

## 1. Introduction


### 1.1 Purpose
-------
This document provides an overview about the architecture of the application. It contains class diagrams and visualizes the architecture of the database as well.


### 1.2 Scope
-------
This document describes the architecture of the Orchestra-application and its data-structure.

### 1.3 Definitions, Acronyms and Abbreviations
-------
| **Abbreviation** | **Description**|
|---|---------------------------------|
|API|Application programming interface|
|MVC|Model View Controller|
|RES|Representational state transfer|
|UC |Use Case|
|N/A|Not Applicable|

### 1.4 References
-------
| **Title** | **Date** |
| --- | --- |
| [Orchestra Blog](https://orchestraapplication.wordpress.com/) | 2019-11-30 |
| [GitLab Source Code](https://gitlab.com/lilolunatic/orchestra-code) | 2019-11-30 |
| [Project Management](https://gitlab.com/lilolunatic/orchestra/-/issues) | 2019-11-30 |
| [Software Requirements Specification](../../SRS.md)  | 2019-11-30 |
| [codacy-Repo](https://app.codacy.com/manual/lilolunatic/orchestra-code/dashboard) | 2020-06-28 |

### 1.5 Overview
-------
This document contains the architectural representation, goals and constraints as well as logical, deployment and data views.

## 2. Architectural Representation

In the application the MVC-architecture is implemented in general by using the Django Framework. We're building a REST API that connects our Python backend with our React frontend.

## 3. Architectural Goals and Constraints

The goal is to set up a clean architecture that is easy to maintain later on. The different tasks that need to be fulfilled by the application shall be separated from each other and there should not be more dependencies than necessary.

## 4. Use-Case View
This is our overall use-case diagram:

![Usecase Diagram](../usecases.png)

### 4.1 Use-Case Realizations
-------
* [UC_Create_Group](https://gitlab.com/lilolunatic/orchestra/blob/master/Documentation/UC/UC_Create_Group.md)
* [UC_Manage_User_Instruments_of_Group](https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/Manage_User_Instruments_of_Group.md)
* [UC_Manage_Users](https://gitlab.com/lilolunatic/orchestra/blob/master/Documentation/UC/UC_Manage_Users.md)
* [UC_Manage_Instruments_of_User](https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_Manage_Instruments_of_User.md)
* [UC_CreateScore](https://gitlab.com/lilolunatic/orchestra/-/blob/master/Documentation/UC/UC_createScore.md)


## 5. Logical View

### 5.1 Overview
-------
The backend is implemented in python, the functionalities are divided into several classes. According to the MVC Model the interface of the application, the database and the processing of data are divided.

Here you can see our overall class Diagramm for the Frontend:

![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/frontend-total.png)

##### View:
These are the graphical React-components:

![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/components.png)

##### Controller:
The actions are dispatched to enter/change the data in the redux-store:
![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/actions.png)
The containers gather and prepare data to hand it over to the graphical components:
![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/containers.png)
The reducers interpret the actions of the application:
![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/reducers.png)
Selectors are used to gather data from the redux-store:
![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/selectors.png)

##### Model:
These are the model Dependencies:

![frontend-total](https://gitlab.com/lilolunatic/orchestra/-/raw/master/Documentation/diagramms/model-dependencies.png)


### 5.2 Architecturally Significant Design Packages
-------
We use the following packages for a quick and easy implementation:
- Django: out Webframework
- Knox: used for token creation and management
- 


## 6. Process View
n/a

## 7. Deployment View
The application will be a web-application and run in the commonly used browsers.

We Deploy out Site with [Heroku](https://www.heroku.com/home).

## 8. Implementation View

### 8.1 Overview
-------
n/a

### 8.2 Layers
-------
n/a

## 9. Data View (optional)
The sheet music will be stored on a database, each user has access to the documents assigned to him or her.

Our ERM-diagram:
![Databse-ERM](../Database-ERM.png)

## 10. Size and Performance
For the storage of data in the application we are using technologies that help us keep the required amount of space small. By using caching, we could also improve performance.

## 11. Quality and Metrics
For the project we chose a commonly used architectural model, that helps us maintain the code and that also makes the application run stable. It also offers many posibilities to extend the application later on. Features like token authentication help us to keep the application safe.
To ensure the code-quality, we use Codacy to measure the code with each commit.
Codacy helps us to keep an eye on duplicated code, it warns us when we have unused code or codestyle-issues, it checks for security issues and more. Furthermore it helps us to keep the complexity of the written code low, so the project stays maintainable.