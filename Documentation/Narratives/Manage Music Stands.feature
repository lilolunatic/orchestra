# Created by nboehm at 29.10.2019
Feature: Manage Music Stands
  In this Use-Case we describe the management of Music Stands.
  That means deciding what accumulation of users can see what sheet music for a specific music score.
  One music score can have multiple sheet music.
  For example "Max Bruch op.88" is the music score and "Max Bruch op.88, violin 1" is a music sheet.
  In this example the instrument is "violin" and the part is "1".
  There is the possibility for a group to define intruments with parts.
  If this is the case, those instruments and parts are always shown, if the conductor opens the score manager for import.

  Scenario Outline: assign defined part for defined instrument
    Given group <groupTitle> exists
    And user <userName> exists

    And <groupTitle> defined the <instrument>
    And <groupTitle> defined for the <instrument> <part1> and <part2>
    And <instrument> <part2> has no file assigned to it

    And <userName> is logged in
    And <userName> is in Group-Lobby of <groupTitle>

    When I enter Score-Manager-Import
    Then <instrument> is shown with <part1> and <part2>
    And <instrument> has no file assigned to <part1> and <part2>

    When I Select the file <fileName> for <instrument> <part2>
    Then <fileName> is assigned to <instrument> <part2>

    Examples:
      | groupTitle | userName | instrument |part1|part2|fileName|
      |OrchestraMusicStands|ConductorMusicStands|trumpet|1|2|trumpet 2|

  Scenario Outline: assign added part for defined instrument
    Given <groupTitle> exists
    And <userName> exists

    And <groupTitle> defined <instrument>
    And <groupTitle> defined for <instrument> <part1> and <part2>

    And <userName> is logged in
    And <userName> is in Score_Manager_Import of <groupTitle>

    When I select add_part_btn for <instrument>
    And I write <part3> into the part_tbox

    And I select the file <fileName> for <instrument> <part3>
    Then <fileName> is assigned to <instrument> <part3>
    Examples:
      | groupTitle | userName | instrument | part1 | part2 | part3 | fileName |
    |OrchestraMusicStands|ConductorMusicStands|trumpet|1|2|3|trumpet 3|

  Scenario: delete added part for defined instrument
    Given group "OrchestraMusicStands" exists
    And user "ConductorMusicStands" exists

    And user "ConductorMusicStands" is logged in
    And user "ConductorMusicStands" is in "Score Manager Import"  of group "OrchestraMusicStands"

    And the group "OrchestraMusicStands" defined the instrument "trumpet"
    And the group "OrchestraMusicStands" defined for the instrument "trumpet" "part 1" and "part 2"
    And for instrument "trumpet" "part 3" is added

    When I delete "trumpet" "part 3"
    Then only "trumpet" "part 3" is deleted

  Scenario: assign added part for added instrument
    Given group group "OrchestraMusicStands" exists
    And user "ConductorMusicStands" exists

    And the group "OrchestraMusicStands" did not define the instrument "trumpet"
    And the group "OrchestraMusicStands" did not define for the instrument "trumpet" "part 1"

    And user "ConductorMusicStands" is logged in
    And user "ConductorMusicStands" is in "Score Manager Import"  of group "OrchestraMusicStands"

    When I select add instrument
    And I add the instrument trumpet

    And I select "add part" for the instrument "trumpet"
    And I write "1" into the part-textbox

    And I select the file "trumpet 1" for the instrument "trumpet" "part 1"
    Then the instrument "trumpet" "part 1" has the file "trumpet 1" assigned to it

  Scenario: delete added part for added instrument
    Given group "OrchestraMusicStands" exists
    And user "ConductorMusicStands" exists

    And user "ConductorMusicStands" is logged in
    And user "ConductorMusicStands" is in "Score Manager Import"  of group "OrchestraMusicStands"

    And in "Score Manager Import" the instrument "trumpet" is added
    And for instrument "trumpet" "part 1" is added

    When I delete "trumpet" "part 1"
    Then only "trumpet" "part 1" is deleted

  Scenario: try to add already existing part to instrument
    Given group "OrchestraMusicStands" exists
    And user "ConductorMusicStands" exists

    And user "ConductorMusicStands" is logged in
    And user "ConductorMusicStands" is in "Score Manager Import"  of group "OrchestraMusicStands"

    And in "Score Manager Import" the instrument "trumpet" is added
    And for instrument "trumpet" "part 1" is added

    When I try to add part "1" for instrument "trumpet"
    Then an error message is shown

  Scenario: cancel assignment
    Given group "OrchestraMusicStands" exists
    And user "ConductorMusicStands" exists

    And user "ConductorMusicStands" is logged in
    And user "ConductorMusicStands" is in "Score Manager Import"  of group "OrchestraMusicStands"

    And in "Score Manager Import" the instrument "trumpet" is added
    And for instrument "trumpet" "part 1" is added

    When I cancel
    Then a warning is shown

  Scenario: save assignemnt
    Given group "OrchestraMusicStands" exists
    And user "ConductorMusicStands" exists

    And user "ConductorMusicStands" is logged in
    And user "ConductorMusicStands" is in "Score Manager Import"  of group "OrchestraMusicStands"

    And in "Score Manager Import" the instrument "trumpet" is added
    And for instrument "trumpet" "part 1" is added
    And for instrument "trumpet" "part 1" the file "trumpet 1" is imported

    When I save score
    Then the score has added for "trumpet" "part 1" the file "trumpet 1"
