# Created by nboehm at 03.11.2019
Feature: Create Account
 An user can create an account.

  Scenario: user gives an account a valid name
    Given user is in Register
    When user defines a name
    And the name is valid
    And the user tries to create account
    Then the user is shown the Password Creator

  Scenario: user tries to create an account with invalid name
    Given user is in Register
    When user defines a name
    And the name is not valid
    And the user tries to create an account
    Then an error message is shown to the user

  Scenario: user tries to define invalid password
    Given user is in Register
    And the Password Creator is shown
    When the user defines a password, that is invalid
    And the user creates accuont
    Then an error message is shown to the user

  Scenario: user defines valid password
    Given user is in Register
    And the Password Creator is shown
    When the user defines a password, that is valid
    And the user creates accuont
    Then the user is shown, that an account is succesfully created