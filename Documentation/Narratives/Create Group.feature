# Created by nboehm at 29.10.2019
Feature: Create Group
  Every user can create a new group. A group is an aggregation of users,
  that play music together. The Group Creator is the ui to create a group.
  In the Group Creator you have to add a group title.
  Additionally you can add users to the group and give them special permissions.

  Scenario: Create Group with title "Orchestra Test Title"
    Given user "ConductorCreateGroup" exists
    And user "ConductorCreateGroup" is logged in
    And user "ConductorCreateGroup" is in Group Creator

    When I enter GroupTitle "Orchestra Test Title"
    And I save Group
    Then the group "Orchestra Test Title" is created

  Scenario: Save Orchestra with 2 users
    Given user "testUser1" exists
    And user "testUser2" exists
    And user "ConductorCreateGroup" exists
    And user "ConductorCreateGroup" is logged in
    And user "ConductorCreateGroup" is in Group Creator
    And GroupTitle "Orchestra2Users" is entered

    When I write the "testUser1" into latest User-Textbox
    And I finish username entry
    Then "testUser1" is added to userlist

    When I write the "testUser2" into latest User-Textbox
    And I finish username entry
    Then "testUser2" is added to userlist

    When I save Group
    Then the group "Orchestra2Users" with "testUser1" and "testUser2" is created

  Scenario: Cancel Create Group
    Given user "ConductorCreateGroup" exists
    And user "ConductorCreateGroup" is logged in
    And user "ConductorCreateGroup" is in GroupCreator

    When I write into GroupTitle-Textbox "OrchestraCancel"
    And I cancel
    Then the group "OrchestraCancel" is not created

  Scenario: add Musician
    Given user "ConductorCreateGroup" exists
    And user "ConductorCreateGroup" is logged in
    And user "ConductorCreateGroup" is in GroupCreator
    And user "testUserPermissions" exists

    When I write "testUserPermissions" into latest User-Textbox
    And I finish the User-Textbox entry
    Then the user "testUserPermissions" is shown in list with permission "Musician"

  Scenario: add Music Admin
    Given user ConductorCreateGroup exists
    And user ConductorCreateGroup is logged in
    And user ConductorCreateGroup is in GroupCreator
    And user "testUserPermissions" exists
    And user "testUserPermissions" is written into latest User-Textbox and valid

    When I select the permission music admin
    Then the user "testUserPermissions" is shown in list with permission "Music Admin"

 Scenario: add second Conductor
   Given user ConductorCreateGroup exists
   And user ConductorCreateGroup is logged in
   And user ConductorCreateGroup is in GroupCreator
   And user "testUserPermissions" exists
   And user "testUserPermissions" is written into latest User-Textbox and valid

   When I select the permission "Conductor"
   Then the user "testUserPermissions" is shown in list with permission "Conductor"

 Scenario: try to add non existing user
  Given user "testUser1" does not exist
  And user "ConductorCreateGroup" exists
  And user "ConductorCreateGroup" is logged in
  And user "ConductorCreateGroup" is in Group Creator

  When I write "testUser1" into latest User-Textbox
  And I finish username entry
  Then an error message is shown

 Scenario: try to add an user, that is added already
   Given user "testUser1" is already added in group
   And user "ConductorCreateGroup" exists
   And user "ConductorCreateGroup" is logged in
   And user "ConductorCreateGroup" is in Group Creator

   When I write "testUser1" into latest User-Textbox
   And I finish username entry
   Then an error message is shown

 Scenario: try to name a group with an existing group title
   Given the group "existingOrchestra" exists
   And user "ConductorCreateGroup" exists
   And user "ConductorCreateGroup" is logged in
   And user "ConductorCreateGroup" is in Group Creator

   When I write the group title "existingOrchestra" into GroupTitel-Textbox
   And I finish the entry into GroupTitel-Textbox
   Then an error message is shown

 Scenario: try to save group without a group title
   Given user "ConductorCreateGroup" exists
   And user "ConductorCreateGroup" is logged in
   And user "ConductorCreateGroup" is in Group Creator
   And the group title is empty

   When I save the group
   Then an error message is shown