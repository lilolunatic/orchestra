# Created by nboehm at 03.11.2019
Feature: Manage Instruments of User
  The user deposits in their account, what instruments they are generally playing.
  There is a global list of instruments, where the user can choose from.

  Scenario: user views list of instruments
    Given the user exists
    And the user is logged in
    And the user deposited some instruments in their account
    And the user is on "Dashboard"
    When the user goes to "Manage my Instruments"
    Then a List of all deposited instrument of that user is shown to the user

  Scenario: user adds instrument
    Given the user exists
    And the user is logged in
    And the user is on "Manage my Instruments"
    When the user chooses Instrument from global instrument list
    Then this instrument is added to the users account
    And "Manage my Instruments" is shown with the chosen instrument

  Scenario: user goes to "Instrument Deletion"
    Given the user exists
    And the user is logged in
    And a specific instrument is deposited in users account
    And the user is on "Manage my Instruments"
    When the user deletes the specific instrument
    Then the "Instrument Deletion" is shown
    And a warning is shown: "All personal memos for that instrument will be deleted. Are you sure you want to delete this instrument from your account?"
    And the user is shown the option to cancel and the option to delete

  Scenario: user deletes specific instrument
    Given the user exists
    And the user is logged in
    And a specific instrument is deposited in users account
    And the user is on "Instrument Deletion"
    When the user chooses the option to delete
    Then the instrument is deleted from their account
    And "Manage my Instruments" is shown without that instrument

 Scenario: user cancels deletion of specific instrument
    Given the user exists
    And the user is logged in
    And a specific instrument is deposited in users account
    And the user is on "Instrument Deletion"
    When the user chooses the option to cancel
    Then the instrument is not deleted from their account
    And "Manage my Instruments" is shown with that instrument

 Scenario: user exits "Manage my Instruments"
   Given the user exists
   And the user is logged in
   And the user is on "Manage my Instruments"
   When the user exits "Manage my Instruments"
   Then the user is on "Dashboard"